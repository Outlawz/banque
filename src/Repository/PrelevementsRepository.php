<?php

namespace App\Repository;

use App\Entity\Prelevements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Prelevements|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prelevements|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prelevements[]    findAll()
 * @method Prelevements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrelevementsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Prelevements::class);
    }

    // /**
    //  * @return Prelevements[] Returns an array of Prelevements objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prelevements
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
