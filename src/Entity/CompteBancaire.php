<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CompteBancaireRepository")
 */
class CompteBancaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numeroCompte;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $montant;

    /**
     * @ORM\Column(type="float")
     */
    private $decouvert;

    /*
     * -------------------------------------------------------------------------------------------------------
     *                  Les opérations du compte bancaire sont stockés dans les tables respectives suivantes
     * -------------------------------------------------------------------------------------------------------
     */

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Prelevements", mappedBy="compteBancaire")
     */
    private $prelevements;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Virement", mappedBy="compteBancaire")
     */
    private $virements;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="comptes")
     */
    private $proprietaire;

    public function __construct()
    {
        $this->prelevements = new ArrayCollection();
        $this->virements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroCompte(): ?string
    {
        return $this->numeroCompte;
    }

    public function setNumeroCompte(string $numeroCompte): self
    {
        $this->numeroCompte = $numeroCompte;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMontant()
    {
        return $this->montant;
    }

    public function setMontant($montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getDecouvert()
    {
        return $this->decouvert;
    }

    public function setDecouvert($decouvert): self
    {
        $this->decouvert = $decouvert;

        return $this;
    }

    /**
     * @return Collection|Prelevements[]
     */
    public function getPrelevements(): Collection
    {
        return $this->prelevements;
    }

    public function addPrelevement(Prelevements $prelevement): self
    {
        if (!$this->prelevements->contains($prelevement)) {
            $this->prelevements[] = $prelevement;
            $prelevement->setCompteBancaire($this);
        }

        return $this;
    }

    public function removePrelevement(Prelevements $prelevement): self
    {
        if ($this->prelevements->contains($prelevement)) {
            $this->prelevements->removeElement($prelevement);
            // set the owning side to null (unless already changed)
            if ($prelevement->getCompteBancaire() === $this) {
                $prelevement->setCompteBancaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Virement[]
     */
    public function getVirements(): Collection
    {
        return $this->virements;
    }

    public function addVirement(Virement $virement): self
    {
        if (!$this->virements->contains($virement)) {
            $this->virements[] = $virement;
            $virement->setCompteBancaire($this);
        }

        return $this;
    }

    public function removeVirement(Virement $virement): self
    {
        if ($this->virements->contains($virement)) {
            $this->virements->removeElement($virement);
            // set the owning side to null (unless already changed)
            if ($virement->getCompteBancaire() === $this) {
                $virement->setCompteBancaire(null);
            }
        }

        return $this;
    }

    public function getProprietaire(): ?Client
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Client $proprietaire): self
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }
}
